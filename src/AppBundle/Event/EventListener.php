<?php namespace AppBundle\Event;

use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\HttpKernel;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\EventDispatcher\Event;
use AppBundle\Entity\Logs;
use AppBundle\Repository\LogsRepository;
use HttpRequest;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\ORM\EntityManager;

/**
 * Class ${NAME}
 * @author Max Frolov <frmaxm@gmail.com>
 * Date: 08.11.17
 */
class EventListener extends EventDispatcher
{
    protected $container;
    public $entityManager;

    /**
     * EventListener constructor.
     * @param ContainerInterface $container
     * @param EntityManager $entityManager
     */
    public function __construct(ContainerInterface $container, EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
        $this->container = $container;
    }

    /**
     * @param GetResponseEvent $event
     */
    public function onKernelRequest(GetResponseEvent $event)
    {
        $kernel    = $event->getKernel();
        $request   = $event->getRequest();
        $container = $this->container;

//        var_dump(
//
//
//        );
//        die;
    }

    /**
     * @param FilterResponseEvent $event
     */
    public function onKernelResponse(FilterResponseEvent $event)
    {
        $response  = $event->getResponse();
        $request   = $event->getRequest();
        $kernel    = $event->getKernel();
        $container = $this->container;

        if($request->query->get('flag') === 'on'){
            if($this->entityManager->isOpen()){
                (new LogsRepository($this->entityManager, $this->entityManager->getClassMetadata('AppBundle:Logs')))->create($this->setLogData($request, $response));
            }
        }
    }

    public function dispatch($eventName, Event $event = null)
    {
        return parent::dispatch($eventName, $event);
    }

    /**
     * @param $request
     * @param $response
     * @return Logs
     */
    private function setLogData($request, $response){
        $log = new Logs();
        $log->setRequest(json_encode(['headers' => (array)$request->headers->all()])); //'body' => (array)$request->getContent()
        $log->setAnswer(json_encode(['headers' => (array)$response->headers->all()])); //, 'body' => (array)$response->getContent()
        $log->setStatus($response->getStatusCode());
        $log->setIpAddress($request->getClientIp());
        $log->setCreatedAt(new \DateTime('now'));
        $log->setUpdatedAt(new \DateTime('now'));
        return $log;
    }

}