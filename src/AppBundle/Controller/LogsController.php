<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class LogsController extends Controller
{
    /**
     * @Route("http-log")
     */
    public function indexAction()
    {
        die('http-logs');
        return $this->render('AppBundle:Logs:index.html.twig', array(
            // ...
        ));
    }

}
