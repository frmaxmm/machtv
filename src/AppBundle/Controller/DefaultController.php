<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Logs;
use AppBundle\Repository\LogsRepository;
use APY\DataGridBundle\Grid\Source\Entity;
use APY\DataGridBundle\Grid\Mapping\Source;
use Prezent\Grid\GridType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Prezent\Grid\Grid;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction()
    {
        return $this->render('default/index.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
        ]);
    }

    /**
     * @Route("/view/{id}")
     */
    public function viewAction($id)
    {
        return $this->render('default/index.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
        ]);
    }

    /**
     * @Route("/admin/http-log")
     */
    public function logAction(Request $request)
    {
        $source = new Entity('AppBundle:Logs');
        $grid = $this->get('grid');

        $grid->setSource($source);

        if($request->isMethod('post')){
            $grid->handleRequest($request);
        }

        return $grid->getGridResponse('default/grid.html.twig', ['grid' => $grid]);
    }
}
